package inf101h22.treedrawer.view;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JToggleButton;

/**
 * Draws the panel containing the reset button
 */
public class ButtonsPanel extends JComponent {

    JButton reset = new JButton("Reset");
    JButton undo = new JButton("Undo");
    JToggleButton auto = new JToggleButton("Auto");

    /** Create a new ButtonsPanel object */
    public ButtonsPanel() {
        this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        this.add(this.auto);
        this.add(Box.createHorizontalGlue());
        this.add(this.reset);
        this.add(this.undo);
    }

    /** Gets the button for reset */
    public JButton getResetButton() {
        return this.reset;
    }

    /** Gets the button for undo */
    public JButton getUndoButton() {
        return this.undo;
    }

    /** Gets the toggle button for auto mode */
    public JToggleButton getAutoButton() {
        return this.auto;
    }
}
