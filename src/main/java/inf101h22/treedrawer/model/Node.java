package inf101h22.treedrawer.model;

/**
 * Node is an immutable class where objects represents a node in the tree.
 */
public class Node {
    public final double x;
    public final double y;
    public final Node neighbor;

    /** Creates a new Node object. */
    public Node(double x, double y, Node neighbor) {
        this.x = x;
        this.y = y;
        this.neighbor = neighbor;
    }
}
