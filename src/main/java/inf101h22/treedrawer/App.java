package inf101h22.treedrawer;

import javax.swing.JFrame;

import inf101h22.treedrawer.controller.TreeCanvasController;
import inf101h22.treedrawer.model.TreeCanvas;
import inf101h22.treedrawer.view.MainView;

/**
 * The main class for the TreeDrawer app.
 */
public class App {
    
    public static void main(String[] args) {
        new App();
    }

    public App() {
        JFrame frame = new JFrame("INF101 TreeDrawer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        TreeCanvas model = new TreeCanvas();
        MainView view = new MainView(model);
        new TreeCanvasController(model, view);

        frame.setContentPane(view);
        frame.pack();
        frame.setVisible(true);
    }
}
